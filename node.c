#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include "utils.h"

/**
 * 2.2
 */
Node* createNode()
{
    //Allocation de notre structure
    Node* node = (Node*) malloc(sizeof(Node));

    if(node == NULL)
    {
        return NULL;
    }

    //On alloue les valeurs comme le veut l'énoncé
    node->value = initNumber();
    node->left = NULL;
    node->right = NULL;

    return node;
}

/**
 * 2.3
 */
void displayNode(Node* node)
{
    printf("\nNoeud : %d", node->value);
}

/**
 * 2.4
 */
Node* addNodeInto(Node* currentNode, Node* newNode)
{
    //Permet d'affecter un nouvel élement à notre arbre
    if(currentNode == NULL)
    {
        return newNode;
    }

    //Si c'est plus grand, on va sur la partie droite de notre arbre
    if(newNode->value < currentNode->value)
    {
        currentNode->left = addNodeInto(currentNode->left, newNode);
    }

    //Si c'est plus grand, on va sur la partie droite de notre arbre
    if(newNode->value > currentNode->value)
    {
        currentNode->right = addNodeInto(currentNode->right, newNode);
    }

    return NULL;
}

/**
 * 2.5
 */
void browsePre(Node* node)
{
    if(node != NULL)
    {
        displayNode(node);
        browsePre(node->left);
        browsePre(node->right);
    }
}

/**
 * 2.6
 */
void browseIn(Node* node)
{
    if(node != NULL)
    {
        browsePre(node->left);
        displayNode(node);
        browsePre(node->right);
    }
}

/**
 * 2.7
 */
void browsePost(Node* node)
{
    if(node != NULL)
    {
        browsePre(node->left);
        browsePre(node->right);
        displayNode(node);
    }
}

/**
 * 2.8
 */
Node* findNode(Node* node, int value)
{
    if(node != NULL)
    {
        if(node->value == value)
        {
            return node;
        }

        if(node->value < value)
        {
            findNode(node->left, value);
        }

        if(node->value > value)
        {
            findNode(node->right, value);
        }
    }

    return NULL;
}