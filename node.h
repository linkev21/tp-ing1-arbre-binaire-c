#ifndef UNTITLED_NODE_H
#define UNTITLED_NODE_H

/**
 * 2.1
 */
typedef struct Node Node;
struct Node
{
    unsigned int value;

    Node* left;
    Node* right;
};

/**
 * 2.2
 */
Node* createNode();

/**
 * 2.3
 */
void displayNode(Node* node);

/**
 * 2.4
 */
Node* addNodeInto(Node* currentNode, Node* newNode);

/**
 * 2.5
 */
void browsePre(Node* node);

/**
 * 2.6
 */
void browseIn(Node* node);

/**
 * 2.7
 */
void browsePost(Node* node);

/**
 * 2.8
 */
Node* findNode(Node* node, int value);

#endif //UNTITLED_NODE_H
