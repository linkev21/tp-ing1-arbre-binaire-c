#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "node.h"

int main()
{
    //initialisation du générateur aléatoire
    srand(time(NULL));

    //Création du sommet de l'arbre
    Node* root = createNode();

    int countValues = 0;

    printf("Nombre d'élement ? \n");
    scanf("%d", &countValues);

    //Initialisation de l'arbre
    for(int i = 1; i < countValues; i++)
    {
        //Création d'un élement
        Node* newNode = createNode();
        //Ajout de l'élement dans l'arbre
        addNodeInto(root, newNode);
    }

    printf("\n\nParcours préfixé");
    browsePre(root);

    printf("\n\nParcours infixé\n");
    browseIn(root);

    printf("\n\nParcours postfixé");
    browsePost(root);

    Node* foundNode = findNode(root, 78); //on peut mettre ce qu'on veut ici, juste comprise entre 50 et 100

    if(foundNode)
    {
        printf("%d", foundNode->value);
    }
    else
    {
        printf("\n\nElement introuvable");
    }

    return 0;
}
